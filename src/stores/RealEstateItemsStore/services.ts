import baseService from "../../services/baseService";
import {RealEstateItemsSearchResponseItem} from "./response.types";

export function getRealEstateItems(filters: any, limit: number, controller?: AbortController) {
    return baseService.get<RealEstateItemsSearchResponseItem[]>("/items", {
        params: {
            ...filters,
            "_limit": limit
        },
        signal: controller?.signal
    })
}