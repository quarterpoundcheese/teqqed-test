type RealEstateItemsSearchResponseItem = {
    id: number,
    thumb: string,
    price: number,
    vicinity: string,
    city: string,
    country: string,
    extra_information: string[],
    date_added: string,
    type: string,
}

export type {RealEstateItemsSearchResponseItem}