import RealEstateItem from "./RealEstateItem.interface";
import {RealEstateItemsSearchResponseItem} from "./response.types";

export function realEstateItemsSearchResponseItemsMapper(data: RealEstateItemsSearchResponseItem[]): RealEstateItem[] {
    return data.map((item) => ({
        ...item,
        location: {
            vicinity: item.vicinity,
            city: item.city,
            country: item.country,
        },
        dateAdded: new Date(item.date_added),
        extraInformation: item.extra_information
    }))
}