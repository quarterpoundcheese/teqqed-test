interface RealEstateItem {
    id: number,
    thumb: string,
    price: number,
    location: {
        vicinity: string,
        city: string,
        country: string,
    },
    dateAdded: Date,
    extraInformation: string[],
    type: string,
}

export default RealEstateItem;

