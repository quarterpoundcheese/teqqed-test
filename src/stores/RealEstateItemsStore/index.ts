import {action, makeAutoObservable, observable, reaction, computed} from "mobx";

import RealEstateItem from "./RealEstateItem.interface";
import {RootStore} from "../store";
import {getRealEstateItems} from "./services";
import {realEstateItemsSearchResponseItemsMapper} from "./response.mappers";
import {AxiosError} from "axios";
import {makeParams} from "./utils";

export interface IFilters {
    search: string;
    sort_by: string;
    type: string;
    page: number;
}

export interface IRealEstateItemsStore {
    items: RealEstateItem[];
    isLoading: boolean;
    total: number;
    filters: IFilters;
    searchError?: AxiosError;
    abortController?: AbortController;

}


export default class RealEstateItemsStore implements IRealEstateItemsStore {
    private rootStore?: RootStore;
    readonly defaultFilters = {
        search: "",
        sort_by: "",
        type: "",
        page: 1,
    };

    private readonly filterExclusions = [
        'page'
    ]

    readonly limit: number = 10;
    abortController: AbortController | undefined;

    @observable isLoading = false;
    @observable total = 0;


    @observable filters = {...this.defaultFilters}
    @observable searchError = undefined;
    @observable items: RealEstateItem[] = [];

    constructor(rootStore?: RootStore) {
        this.rootStore = rootStore
        makeAutoObservable(this)

        
        reaction(
            () => Object.keys(this.filters).map(filter => (this.filters as any)[filter]),
            (f) => {

                this.loadItems()
            }
        )

    }

    @action resetToDefaultFilters() {
        this.isLoading = true;
        this.filters = {...this.defaultFilters}
    }

    @action resetPagination() {
        this.filters.page = this.defaultFilters.page;
    }

    @computed get filtersCount() {
        const keys = Object.keys(this.filters);
        let count = 0;
        keys.forEach(t => {
            if(!this.filterExclusions.includes(t) && (this.defaultFilters as any)[t] !== (this.filters as any)[t]) {
                count++;
            }
        })

        return count;
    }

    @action loadItems(filters?: any) {
        this.isLoading = true;
        this.searchError = undefined;

        const params = makeParams(filters || this.filters)

        this.abortController && this.abortController.abort();
        this.abortController = new AbortController();

        getRealEstateItems(params, this.limit, this.abortController).then(r => {
            this.items = realEstateItemsSearchResponseItemsMapper(r.data);
            this.total = parseInt(({...r.headers})['x-total-count'] ?? r.data.length);
        })
        .catch(e => {
            this.searchError = e;
            this.items = [];
            this.total = 0;
        })
        .finally(() => {
            this.isLoading = false;
        })
    }
}