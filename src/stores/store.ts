import RealEstateItemsStore from "./RealEstateItemsStore";
import {configure, makeAutoObservable} from "mobx";

configure({
    enforceActions: "never"
})

export class RootStore {
    realEstateItemStore: RealEstateItemsStore;
    constructor() {
        makeAutoObservable(this)
        this.realEstateItemStore = new RealEstateItemsStore()
        this.realEstateItemStore.loadItems()
    }
}