import { useContext } from 'react';
import { RootStore } from './store';
import { StoreContext } from './storeProvider'

export const useStores = (): RootStore => useContext(StoreContext);