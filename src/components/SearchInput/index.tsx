import Input from "common/Input";
import Button from 'common/Button'
import styles from './SearchInput.module.scss';
import { ReactComponent as SearchIcon } from "assets/search.svg";

interface SearchButtonPropsI {
    disabled?: boolean;
    isLoading?: boolean;
    onClick?: () => void;
    onEnter?: () => void;
}

interface SearchInputPropsI extends SearchButtonPropsI {
    value?: string,
    onChange?: (val: string) => void,
    hideButton: boolean
}

const SearchButton = (props: SearchButtonPropsI) => {
    return (
        <Button {...props} wrapperClass={styles.searchButton}>
            Search
        </Button>
    )
}


export default function SearchInput({ disabled, isLoading, onChange, onClick, onEnter, value, hideButton }: SearchInputPropsI) {

    return (
        <div className={styles.container}>
            <Input
                value={value ?? ""}
                onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                        onEnter && onEnter();
                    }
                }}

                disabled={disabled}
                onChange={e => onChange && onChange(e.target.value)}
                isLoading={isLoading}
                icon={<SearchIcon />}
                rightAddon={hideButton ? undefined : <SearchButton isLoading={isLoading} onClick={onClick} disabled={disabled} />}
                placeholder={"Plaats, buurt, adres, etc."}
                style={{ width: "100%" }} />
        </div>
    )
}