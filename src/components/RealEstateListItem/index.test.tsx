import { render, screen } from "@testing-library/react"
import IRealEstateItem from "stores/RealEstateItemsStore/RealEstateItem.interface"
import { realEstateItemsSearchResponseItemsMapper } from "stores/RealEstateItemsStore/response.mappers"
import RealEstateItem from "."
import fakeData from '../../../fakeData/db.json'

describe("RealEstateListItem", () => {
    let item: IRealEstateItem

    beforeEach(() => {
        item = realEstateItemsSearchResponseItemsMapper(fakeData.items)[0]
    })


    test("it works", async () => {
        render(<RealEstateItem item={item} />)
        const textFound = await screen.findByText(`${item.type} in ${item.location.city}`);
        expect(textFound).toBeVisible()
    })
})

export {}