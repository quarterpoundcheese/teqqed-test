import styles from './RealEstateListItem.module.scss';
import { ReactComponent as PinSvg } from "assets/pin.svg";
import RealEstateItemInterface from "stores/RealEstateItemsStore/RealEstateItem.interface";
import classNames from "classnames";
import { useMemo } from "react";
import Highlighter from 'common/Highlighter';
import {formatPrice} from "utils";

interface IRealEstateItemProps {
    item?: RealEstateItemInterface,
    search?: string,
}


export default function RealEstateItem({ item, search }: IRealEstateItemProps) {

    const title = useMemo(() => item && `${item.type} in ${item.location.city}`, [item]);
    const location = useMemo(() => item && `${item.location.vicinity}, ${item.location.city}, ${item.location.country}`, [item])
    const extraInformation = useMemo(() => item ? item.extraInformation : [], [item])
    const price = useMemo(() => item && formatPrice(item.price), [item])

    return (
        <div className={styles.container}>
            <div style={{ ...(item && { backgroundImage: `url(${item.thumb})` }) }} className={styles.imageContainer} />
            <div className={styles.detailsContainer}>
                <h5
                    className={classNames(styles.title, {
                        [styles.isLoading]: !item
                    })}
                >
                    <Highlighter value={title} search={search} />
                </h5>
                <div className={classNames(styles.location, {
                    [styles.isLoadingShort]: !item
                })}>
                    {item && <PinSvg />}
                    <span>
                        <Highlighter value={location} search={search}/>
                    </span>
                </div>
                <div className={classNames(styles.realEstateStats, {
                    [styles.isLoadingShort]: !item
                })}>
                    <ul>
                        {
                            extraInformation.map((t, i) => (
                                <li key={i}><Highlighter value={t} search={search}/></li>
                            ))
                        }
                    </ul>
                </div>
                <h5 className={styles.price}>{item && '€'} {price}</h5>
            </div>
        </div>
    )
}