import styles from './Layout.module.scss'
import {ReactNode} from "react";

interface ILayoutProps {
    children: ReactNode
}

export default function Layout({children}: ILayoutProps) {
    return (
        <div className={styles.container}>
            {children}
        </div>
    )
}