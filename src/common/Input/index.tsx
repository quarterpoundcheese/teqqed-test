import styles from './Input.module.scss';
import {FocusEvent, InputHTMLAttributes, ReactElement, useCallback, useState} from "react";
import classNames from "classnames";

interface InputPropsI extends InputHTMLAttributes<HTMLInputElement> {
    wrapperClass?: string
    icon?: ReactElement
    rightAddon?: ReactElement
    isLoading?: boolean
}


export default function Input({wrapperClass, isLoading, icon, rightAddon, disabled, ...rest}: InputPropsI) {

    const [isFocused, setIsFocused] = useState(false);

    const onFocus = useCallback((event: FocusEvent<HTMLInputElement, Element>) => {
        setIsFocused(true);
        if(rest.onFocus) rest.onFocus(event);
    }, [rest])

    const onBlur = useCallback((event: FocusEvent<HTMLInputElement, Element>) => {
        setIsFocused(false);
        if(rest.onBlur) rest.onBlur(event);
    }, [rest])

    return (
        <div className={classNames(styles.inputContainer, {
            [styles.inputContainerFocused]: isFocused,
            [styles.inputDisabled]: disabled || isLoading,
        })}>
            {
                icon && (
                    <div className={classNames(styles.inputLeftAddonIcon, {
                        [styles.inputLeftAddonIconDisabled]: disabled || isLoading
                    })}>
                        {icon}
                    </div>
                )
            }
            <input {...rest} disabled={disabled || isLoading} onBlur={onBlur} onFocus={onFocus} className={classNames(styles.input, wrapperClass, {
                [styles.inputLeftAddon]: icon
            })}/>
            {
                rightAddon && (
                    <div>
                        {rightAddon}
                    </div>
                )
            }
        </div>
    )
}