export default interface SelectItem {
    value: string | number;
    label: string;
}