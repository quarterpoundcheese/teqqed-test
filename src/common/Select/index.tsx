import styles from './Select.module.scss';
import ReactSelect, {StylesConfig, ContainerProps, GroupBase} from "react-select";
import {ReactComponent as DownSvg} from "assets/down.svg";
import classNames from "classnames";
import {InputHTMLAttributes} from "react";
import SelectItem from "./types";

interface SelectProps extends InputHTMLAttributes<HTMLInputElement> {
    options: SelectItem[],
    label: string,
    wrapperClass?: string,
    onChange?: (e: any) => void,
    isClearable?: boolean,
}

const DropdownIndicator = () => (
    <DownSvg className={styles.dropDownIndicator}/>
)

const customStyles: StylesConfig = {
    container: (base, props) => ({
        ...base,
        width: "100%",

    }),
    control: (base, props) => ({
        ...base,
        fontFamily: "'Roboto', sans-serif",
        fontSize: "16px",
        boxShadow: "none",
        width: "100%",
        border: "none",
        borderLeft: "none",
        borderRadius: "0px",
        padding: "8px 0",
        cursor: 'pointer'
    }),
    menu: (base) => ({
        ...base,
        boxShadow: "0px 4px 30px rgba(0, 0, 0, 0.15)",
        borderRadius: "8px",
        marginTop: "15px",
        width: "calc(102% + 4.5em)",
        padding: "12px 0",
        left: "-4.5em"
    }),
    menuPortal: (base) => ({
        ...base,
        zIndex: '999'
    }),
    option: (base, state) => {
        let backgroundColor = "white";
        let color = "#191C1F";
        if (state.isFocused) {
            backgroundColor = "rgba(236, 127, 7, 0.1)";
        }

        if (state.isSelected) {
            backgroundColor = "#EC7F07";
            color = "white";
        }

        return (
            {
                ...base,
                backgroundColor,
                color,
                fontFamily: "Roboto",
                ":active": {
                    backgroundColor: "#EC7F07",
                    color: "white"
                },
                cursor: 'pointer',
            }
        )
    },
}

export default function Select(
    {
        options,
        label,
        wrapperClass,
        placeholder,
        value,
        onChange,
        isClearable,
        ...rest
    }: SelectProps) {
    return (
        <ReactSelect
            placeholder={placeholder}
            value={options.filter(t => t.value === value)}
            openMenuOnFocus
            isClearable={isClearable}
            onChange={onChange}
            menuPortalTarget={document.body}
            components={
                {
                    DropdownIndicator,
                    IndicatorSeparator: () => null,
                    SelectContainer: (props: ContainerProps<unknown, boolean, GroupBase<unknown>>) => {
                        return (
                            <div {...rest} className={classNames(styles.container, wrapperClass)}>
                                <span className={styles.label}>{label}</span>
                                {props.children}
                            </div>
                        )
                    }
                }
            }
            styles={customStyles}
            aria-label={"hello"}
            options={options}/>
    )
}