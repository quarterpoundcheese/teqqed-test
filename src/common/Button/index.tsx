import styles from './Button.module.scss';
import {ButtonHTMLAttributes} from "react";
import classNames from "classnames";

import {ReactComponent as LoadingIcon} from "../../assets/loading.svg";

interface ButtonPropsI extends ButtonHTMLAttributes<HTMLButtonElement> {
    wrapperClass?: string
    isLoading?: boolean
}

export default function Button({wrapperClass, children, disabled, isLoading, ...rest}: ButtonPropsI) {

    return (
        <button type="button" {...rest} disabled={disabled || isLoading} className={classNames(styles.button, wrapperClass)} >
            {
                isLoading ? (
                    <LoadingIcon className={styles.buttonLoading} />
                ) : children
            }
        </button>
    )
}