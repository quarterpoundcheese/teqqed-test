interface IHighlighterProps {
    value?: string,
    search?: string,
}

export default function Highlighter({ value, search }: IHighlighterProps) {
    const test = search && search.replace(/[#-}]/g, '\\$&')


    if ((!search || search === "") || (!value || value === "")) {
        return <span style={{ margin: 0, padding: 0 }}>{value}</span>
    }

    const parts = value.split(new RegExp(`(${test})`, 'gi'));
    return <span style={{ margin: 0, padding: 0 }}> {parts.map((part, i) => part.toLowerCase() === search.toLowerCase() ? (
        <mark key={i}>
            {part}
        </mark>
    ) : <span style={{ margin: 0, padding: 0 }} key={i}>{part}</span>)} </span>;

}