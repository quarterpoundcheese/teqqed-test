import { render, screen, waitFor } from "@testing-library/react"
import { RootStore } from "stores/store"
import { StoreProvider } from "stores/storeProvider"
import HomePage from "."


const store = new RootStore();

describe("Home", () => {
    test("Renders with store", () => {
        render(
            <StoreProvider store={store}>
                <HomePage />
            </StoreProvider>
        )


        const foundButton = screen.getByRole("button");
        expect(foundButton).toBeDisabled()
    })

    test("Renders with store and waits for 3 seconds", async () => {
        render(
            <StoreProvider store={store}>
                <HomePage />
            </StoreProvider>
        )
        await waitFor(() => expect(screen.getByRole("button", {name: /Search/i})).toBeEnabled(), {timeout: 3000})
    })
})

export {}