import styles from './Home.module.scss';
import RealEstateItem from "components/RealEstateListItem";
import SearchInput from "components/SearchInput";
import Select from "common/Select";
import {
    DropDownSelect,
    REAL_ESTATE_TYPE_FILTER_VALUES,
    SORT_BY_FILTER_VALUES
} from "stores/RealEstateItemsStore/constants";
import {observer} from "mobx-react";
import {useStores} from "stores/useStore";
import SkeletonLoading from "./Skeleton";
import {useCallback, useState} from "react";
import Button from 'common/Button';
import NoItems from './NoItems';
import Pagination from '@mui/material/Pagination';
import Layout from "common/Layout";
import {useMediaQuery} from "@mui/material";
import {BreakPoints} from "utils";
import RealEstateItemsStore from "stores/RealEstateItemsStore";
import classNames from "classnames";

import {ReactComponent as ChevronDown} from "assets/expand.svg";

enum HomePageFilters {
    SEARCH = 'search',
    PAGE = 'page',
    TYPE = 'type',
    SORT_BY = 'sort_by',
}

interface ISelectFiltersProps {
    onSortByChange: (e: DropDownSelect) => void,
    onTypeChange: (e: DropDownSelect) => void,
    realEstateItemStore: RealEstateItemsStore,
    onReset: () => void,
}

function SelectFilters({onSortByChange, onTypeChange, realEstateItemStore, onReset}: ISelectFiltersProps) {
    return (
        <>
            <Select
                onChange={onSortByChange}
                value={realEstateItemStore.filters.sort_by}
                style={{width: 'auto'}}
                label={"Sort by"}
                options={SORT_BY_FILTER_VALUES}
            />
            <Select
                onChange={onTypeChange}
                value={realEstateItemStore.filters.type}
                style={{marginTop: "15px", width: 'auto'}}
                label={"Type"}
                options={REAL_ESTATE_TYPE_FILTER_VALUES}
            />
            {
                realEstateItemStore.filtersCount !== 0 && (
                    <Button onClick={onReset} style={{width: '100%', marginTop: '15px'}}>
                        Clear {realEstateItemStore.filtersCount} filters
                    </Button>
                )
            }
        </>
    )
}

const HomePage = observer(() => {
    const [search, setSearch] = useState<string>("")
    const {realEstateItemStore} = useStores();

    const isMobile = useMediaQuery(BreakPoints.isDesktop);
    const [isFiltersOpen, setIsFiltersOpen] = useState<boolean>(false);

    const onFiltersChange = useCallback((value: string | number, field: string) => {
        const {filters, defaultFilters} = realEstateItemStore;
        if (field !== HomePageFilters.PAGE) {
            realEstateItemStore.filters = {...filters, page: defaultFilters.page, [field]: value}
            return;
        }
        realEstateItemStore.filters.page = Number(value)

    }, [realEstateItemStore])

    const onSearchButtonClick = useCallback(() => {
        onFiltersChange(search, HomePageFilters.SEARCH)
    }, [onFiltersChange, search]);

    const onSortByChange = useCallback((e: DropDownSelect) => {
        onFiltersChange(e.value, HomePageFilters.SORT_BY)
    }, [onFiltersChange])

    const onTypeChange = useCallback((e: DropDownSelect) => {
        onFiltersChange(e.value, HomePageFilters.TYPE)
    }, [onFiltersChange])

    const onPaginationChange = useCallback((_: any, p: number) => {
        onFiltersChange(p, HomePageFilters.PAGE)
    }, [onFiltersChange])


    const onReset = useCallback(() => {
        realEstateItemStore.resetToDefaultFilters();
        setSearch('')
    }, [realEstateItemStore, setSearch]);



    return (
        <Layout>
            <div className={styles.container}>
                <div className={styles.filterPanel}>
                    {
                        !isMobile && (
                            <div className={styles.filterPanelInner}>
                                <SelectFilters
                                    onReset={onReset}
                                    onSortByChange={onSortByChange}
                                    onTypeChange={onTypeChange}
                                    realEstateItemStore={realEstateItemStore}
                                />
                            </div>
                        )
                    }
                </div>
                <div className={styles.contentPanel}>
                    <SearchInput
                        hideButton={isMobile}
                        value={search}
                        onEnter={onSearchButtonClick}
                        isLoading={realEstateItemStore.isLoading}
                        onClick={onSearchButtonClick}
                        onChange={(val) => setSearch(val)}
                    />
                    {
                        isMobile && (
                            <Button onClick={onSearchButtonClick} style={{marginBottom: "10px", width: "100%"}}>
                                Search
                            </Button>
                        )
                    }
                    {
                        isMobile && (
                            <div className={styles.mobileFilters}>
                                <div className={styles.titleContainer}>
                                    <h4>Filters {realEstateItemStore.filtersCount ? `(${realEstateItemStore.filtersCount} filters selected)` : ""}</h4>
                                    <span onClick={() => setIsFiltersOpen(val => !val)}>
                                        <ChevronDown style={{transition: '200ms'}} className={classNames({
                                            [styles.iconRotated]: isFiltersOpen
                                        })}/>
                                    </span>
                                </div>
                                <div className={classNames(
                                    styles.mobileFiltersInner, {
                                        [styles.mobileFiltersInnerOpen]: isFiltersOpen
                                    }
                                )}>
                                    <SelectFilters
                                        onReset={onReset}
                                        onSortByChange={onSortByChange}
                                        onTypeChange={onTypeChange}
                                        realEstateItemStore={realEstateItemStore}
                                    />
                                </div>
                            </div>
                        )
                    }
                    <div className={styles.realEstateListItemContainer}>
                        {
                            !realEstateItemStore.isLoading && realEstateItemStore.items.length === 0 && (
                                <NoItems isError={!!realEstateItemStore.searchError}/>
                            )
                        }
                        {
                            realEstateItemStore.isLoading && (
                                <SkeletonLoading/>
                            )
                        }
                        {
                            !realEstateItemStore.isLoading && realEstateItemStore.items.map((val) => (
                                <div className={styles.realEstateListItemWrapper} key={val.id}>
                                    <RealEstateItem item={val} search={search}/>
                                </div>
                            ))
                        }
                    </div>
                    {
                        realEstateItemStore.total !== 0 && !realEstateItemStore.searchError && (
                            <div className={styles.pagination}>
                                <Pagination
                                    disabled={realEstateItemStore.isLoading}
                                    onChange={onPaginationChange}
                                    page={realEstateItemStore.filters.page}
                                    size={'small'}
                                    count={Math.floor(realEstateItemStore.total / realEstateItemStore.limit)}
                                />
                            </div>
                        )
                    }
                </div>
            </div>
        </Layout>
    )
})

export default HomePage;