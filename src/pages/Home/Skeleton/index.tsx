import RealEstateItem from "components/RealEstateListItem";
import styles from "../Home.module.scss";

const LOADING_SIZE = 10;

export default function SkeletonLoading() {

    return (
        <>
            {
                Array.from(new Array(LOADING_SIZE).keys()).map((_, k) => (
                    <div className={styles.realEstateListItemWrapper} key={k}>
                        <RealEstateItem/>
                    </div>
                ))
            }
        </>
    )

}