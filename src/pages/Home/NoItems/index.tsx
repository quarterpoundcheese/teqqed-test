import styles from './NoItems.module.scss';


export default function NoItems({isError}: { isError: boolean }) {
    return (
        <div className={styles.container}>
            {
                !isError ? (
                        <div>
                            <h3>Sorry, we couldn't find any items that match the criteria</h3>
                            <h3>Please adjust the filters</h3>
                        </div>
                    )
                    : (
                        <div>
                            <h3>Internal server error :(</h3>
                            <h3>Please refer to the README.md file on how to set up json server</h3>
                        </div>
                    )
            }
        </div>
    )
}