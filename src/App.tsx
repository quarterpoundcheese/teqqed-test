import React from 'react';
import "./styles/global.scss"
import HomePage from "./pages/Home";

function App() {
    return (
        <div>
            <HomePage />
        </div>
    );
}

export default App;
