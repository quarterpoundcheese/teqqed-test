import axios from "axios";

const baseService = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    headers: {
        "Content-type": "application/json"
    }
});

export default baseService;