# Teqqed Test

## Robot Images

The robot images come from Mockaroo. Some of them fail to load because of the server hosting them

## JSON-Server

I used JSON server for the backend JSON api. https://www.npmjs.com/package/json-server

## .env.example

If you run the application without following the `.env.example`, you will run into a `5xx` eror.

## db.json

Please serve the `db.json` in the root of the project with `json-server`. You can also use https://www.mockaroo.com/ to generate different dummy data

## MOBX

I chose to use MobX for the project because the set-up is much easier than Redux, and requires less boilerplate code
